FROM python:latest
WORKDIR /app
COPY src/grabber.py /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
CMD ["python3", "grabber.py"]
